<?php

  require('connexion_sql.php');


  function recherchetitre(){
    $connexion=connect_bd();
    $titre=$_GET['recherche'];
    $sql="SELECT DISTINCT code_film,titre_original,titre_francais,pays,date,duree,couleur,image,nom,prenom
    from films natural join individus WHERE (titre_original = '".$titre."' or titre_francais = '".$titre."') and films.realisateur=individus.code_indiv";
    $exist="SELECT COUNT(*) from films natural join individus WHERE (titre_original = '".$titre."' or titre_francais = '".$titre."') and films.realisateur=individus.code_indiv";
    if(!$connexion->query($sql)){
      echo "Erreur: Problème d'accès au CARNET";
    }
    else{
       echo "<h1>Voici le resultat de votre recherche</h1>";
       foreach ($connexion->query($exist) as $row){
         $ex=$row[0];
       }
       if ($ex==0) {
         echo "ce film n'est pas dans notre liste";
       }
       else{
       foreach ($connexion->query($sql) as $row)
        echo "<section>";
          echo "<table id=\"f\">";
            echo "<tr><td>Titre original : ".$row['titre_original']."</td>";
            echo "<tr><td>Titre français : ".$row['titre_francais']."</td>";
            echo "<tr><td>année : ".$row['date']."</td>";
            echo "<tr><td>film ".$row['couleur']."</td></tr>";
            echo "<tr><td>Réalisateur : ".$row['prenom']." ".$row['nom']."</td></tr>";
            echo "<tr><td>film ".$row['pays']." durée : ".$row['duree']." min</td></tr>";
          echo "</table>";
        echo "</section>";
      }
  }
}

  function rechercheAnnee(){
    $connexion2=connect_bd();
    $annee=$_GET['rechercheA'];
    $sqlAnnee="SELECT DISTINCT code_film,titre_original,titre_francais,pays,date,duree,couleur,image,nom,prenom
    from films natural join individus WHERE (date >= '".$annee."' and date <='".$annee."'+9) and films.realisateur=individus.code_indiv";
    if(!$connexion2->query($sqlAnnee)){
      echo "Erreur: Problème d'accès au CARNET";
    }
    else{
       echo "<h1>Voici le resultat de votre recherche</h1>";
       echo "<section>";
       foreach ($connexion2->query($sqlAnnee) as $row)
       echo "<section>
          <table>
           <tr><td>Titre original : ".$row['titre_original']."</td>
           <tr><td>Titre français : ".$row['titre_francais']."</td>
           <tr><td>année : ".$row['date']."</td>
           <tr><td>film ".$row['couleur']."</td></tr>
           <tr><td>Réalisateur : ".$row['prenom']." ".$row['nom']."</td></tr>
           <tr><td>film ".$row['pays']." durée : ".$row['duree']." min</td></tr>
         </table>
       </section>\n";
  }
  echo "</section";
}


  function rechercheGenre(){
    $connexion=connect_bd();
    if(isset($_GET['rechercheG'])) {
      $genre=$_GET['rechercheG'];
      $sqlGenre="SELECT DISTINCT code_film,titre_original,titre_francais,pays,date,duree,couleur,image,nom,prenom
      from films natural join classification natural join genres natural join individus WHERE (genres.nom_genre = '".$genre."'
      and genres.code_genre=classification.code_genre and classification.code_film=films.code_film) and films.realisateur=individus.code_indiv";
      if(!$connexion->query($sqlGenre)){
        echo "Erreur: Problème d'accès au CARNET ";
      }
      else{
        echo "<h1>Voici le resultat de votre recherche</h1>";
        echo "<section>";
        foreach ($connexion->query($sqlGenre) as $row)
        echo "<section>
           <table>
            <tr><td>Titre original : ".$row['titre_original']."</td>
            <tr><td>Titre français : ".$row['titre_francais']."</td>
            <tr><td>année : ".$row['date']."</td>
            <tr><td>film ".$row['couleur']."</td></tr>
            <tr><td>Réalisateur : ".$row['prenom']." ".$row['nom']."</td></tr>
            <tr><td>film ".$row['pays']." durée : ".$row['duree']." min</td></tr>
          </table>
        </section>\n";
      }
      echo "</section";
    }
    else {
      echo "Genre vide";
    }
}
  // function ajouteFilm()
  // {
  //   $connexion=connect_bd();
  //   $sqlnbfilms="SELECT COUNT(*) from films";
  //   $sqlnbindiv="SELECT COUNT(*) from individus";
  //   $t_o=$_GET['titre_o'];
  //   $t_f=$_GET['titre_f'];
  //   $nom_real=$_GET['nom_real'];
  //   $prenomreal=$_GET['prenom_real'];
  //   $nationalite=$_GET['nationalite'];
  //   $couleur=$_GET['b1'];
  //   $date=$_GET['date'];
  //   $genre=$_GET['genre'];
  //   $duree=$_GET['duree'];
  //   $sqlfilmexiste="SELECT COUNT(*) from films WHERE titre_original='".$t_o."' and titre_francais='".$t_f."' and date='".$date."'";
  //   $sqlindivexiste="SELECT COUNT(*) from individus WHERE nom='".$nom_real."' and prenom='".$prenomreal."'";
  //   foreach ($connexion->query($sqlnbfilms) as $row){
  //   $nbfilms=$row[0];
  //   }
  //   foreach ($connexion->query($sqlnbindiv) as $row){
  //   $nbindiv=$row[0];
  //   }
  //   foreach ($connexion->query($sqlfilmexiste) as $row){
  //   $nbexiste=$row[0];
  //   }
  //   foreach ($connexion->query($sqlindivexiste) as $row){
  //   $indivexiste=$row[0];
  //   }
    // if ($indivexiste==1) {
    //   $sqlnumreal="SELECT code_indiv from individus WHERE nom='".$nom_real."' and prenom='".$prenom_real."'";
    //   foreach ($connexion->query($sqlnumreal) as $row){
    //   $numreal=$row[0];
    //   }
    // }
    // else {
    //   $numreal=$nbindiv+1;
    //   "INSERT into individus ('code_indiv','nom','prenom') VALUES ('".$numreal."','".$nom_real."','".$prenomreal."')";
    //   echo"dffrgr";
    // }
    // if ($nbexiste!=0) {
    //   echo "ce film est deja dans notre liste de film";
    // }
    // else {
    //   $code=$nbfilms+1;
    //   "INSERT into films ('code_film', 'titre_original', 'titre_francais', 'pays', 'date', 'duree', 'couleur', 'realisateur', 'image') VALUES ('".$code."','".$t_o."','".$t_f."','".$nationalite."','".$date."','".$duree."','".$couleur."','".$numreal."','".$code."'.png)";
    //   "INSERT into classification ('code_film','code_genre') VALUES ('".$code."','".$genre."')";
    //   echo "film ajouté";
    // }


  //
?>
