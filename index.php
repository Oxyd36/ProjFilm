<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8"/>
		<title>Films</title>
		<link rel ="stylesheet" href ="Accueil.css"/>
		<!-- <link href="ressources/index.jpeg">   -->
	</head>
	<body>
		<header>
			<h1>Films en FOlies</h1>
		</header>
		<br><br><br><br><br><br><br>
		<nav>
			<ul>
				<li><p><a href="index.php">ACCUEIL</a></p></li>
				<li><p><a href="ajoutfilm.php">AJOUTER UN FILM</a></p></li>
			</ul>
		</nav>
		<section id='Accueil'>
			<h1>Bienvenue</h1>
			<p>cher visiteur, nous vous souhaitons la bienvenue sur notre site Films en FOlie.</p>

		</section>
		<section>
			<h2> Recherche de films par titre</h2>
				<form action="recherche.php" method="get">
					Recherche
					<input type="text" name="recherche" placeholder="Recherchez un film" size=50 >
					<input type="submit" value="Rechercher">
				</form>
			<h2>Recherche de films par décenie</h2>
			<form action="recherche.php" method="get">
				Recherche
				<select name="rechercheA">
					<option value="">  </option>
					<option value="2010-2019">2010-2019</option>
					<option value="2000-2009">2000-2009</option>
					<option value="1990-1099">1990-1999</option>
					<option value="1980-1089">1980-1989</option>
					<option value="1970-1079">1970-1979</option>
					<option value="1960-1069">1960-1969</option>
					<option value="1950-1059">1950-1959</option>
					<option value="1940-1049">1940-1949</option>
					<option value="1930-1039">1930-1939</option>
					<option value="1920-1029">1920-1929</option>
					<option value="1910-1019">1910-1919</option>
					<option value="1900-1009">1900-1909</option>
					<option value="1890-1899">1890-1899</option>
					<option value="1880-1889">1880-1889</option>
					<option value="1870-1879">1870-1879</option>
				</select>
				<input type="submit" value="Rechercher">
			</form>
			<h2>Recherche de films par genre</h2>
			<form action="recherche.php" method="get">
				Recherche
				<select name="rechercheG">
					<option value="heurs et malheurs à deux heurs et malheurs à deux  ">heurs et malheurs à deux </option>
					<option value="carrément à l''ouest">carrément à l''ouest</option>
					<option value="cadavre(s) dans le(s) placard(s)">cadavre(s) dans le(s) placard(s)</option>
					<option value="c''était demain">c''était demain</option>
					<option value="pas drôle mais beau ">pas drôle mais beau </option>
					<option  value="pauvre espèce humaine ">pauvre espèce humaine </option>
					<option value="du rire aux larmes (et retour)">du rire aux larmes (et retour)</option>
					<option value="portrait d''époque (après 1914)">portrait d''époque (après 1914)</option>
					<option value="en avant la musique">en avant la musique</option>
					<option value="jeu dans le jeu">jeu dans le jeu</option>
					<option value="en France profonde">en France profonde</option>
					<option value="New-York">New-York</option>
					<option value="pour petits et grands enfants">pour petits et grands enfants</option>
					<option value="culte ou my(s)tique">culte ou my(s)tique</option>
					<option value="poésie en image">poésie en image</option>
					<option value="conte de fées relooké">conte de fées relooké</option>
					<option value="les chocottes à zéro">les chocottes à zéro</option>
					<option value="la parole est d''or">la parole est d''or</option>
					<option value="Paris">Paris</option>
					<option value="vive la (critique) sociale !">vive la (critique) sociale !</option>
					<option value="épique pas toc">épique pas toc</option>
					<option value="Los Angeles & Hollywood">Los Angeles & Hollywood</option>
					<option value="perle de nanard">perle de nanard</option>
					<option value="entre Berlin et Moscou">entre Berlin et Moscou</option>
					<option value="à l''antique">à l''antique</option>
					<option value="du Moyen-Age à 1914">du Moyen-Age à 1914</option>
					<option value="docu">docu</option>
					<option value="vers le soleil levant">vers le soleil levant</option>
					<option value="Bollywooderie">Bollywooderie</option>
				</select>
				<input type="submit" value="Rechercher">
			</form>
		</section>
	</body>
</html>
